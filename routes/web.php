<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });



Route::get('/master', function () {
    return view('layouts.master');
 });

Route::get('/', function () {
   $pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('pertanyaan'));
 });


Route::get('/table', function () {
    return view('table');
 });

Route::get('/data_table', function () {
    return view('data_table');
 });

Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{id_pertanyaan}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id_pertanyaan}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{id_pertanyaan}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{id_pertanyaan}', 'PertanyaanController@hapus');
