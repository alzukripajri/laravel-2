@extends('layouts.master')
@section('content')
<div class="m-3">
	<div class="card">
              <div class="card-header">
                <h3 class="card-title">{{ $show->judul }}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              	<p>{{ $show->isi }}</p>
              <!-- /.card-body -->
            </div>
        </div>
    </div>

@endsection